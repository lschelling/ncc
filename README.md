# NETCONF Scripts for albis-elcon ACCEED devices

## Introduction

This repository presents:

* Python scripts using the ncclient library (`0.5.2` or greater as of writing) to talk to NETCONF-enabled devices.
* Jupyter (IPython) Notebooks in the directory [```notebooks```](notebooks). (more infos in [```notebooks/README.md```])

It is a fork of the ```https://github.com/CiscoDevNet/ncc.git``` repository. It contains a collection of scripts that can be used together with the ACCEED devices from albis-elcon.


## System Dependencies (on Ubuntu)

```
$ sudo apt-get install virtualenv python-virtualenv python-dev libssl-dev libxml2-dev libxslt1-dev libffi-dev
```

## Python Dependencies
The package dependencies for the scripts and the jupyter notebooks are listed in ```requirements.txt```, which may be used to install the dependencies thus (note the upgrade to pip; must be running **```pip >= 8.1.2```** to successfully install some dependencies):

```
$ virtualenv v
New python executable in v/bin/python2.7
Also creating executable in v/bin/python
Installing setuptools, pip, wheel...done.
$ . v/bin/activate
(v)$ pip install --upgrade pip
Requirement already up-to-date: pip in ./v/lib/python2.7/site-packages
$ pip install -r requirements.txt
```

It is recommended to isolate the python environment from the global python installation using the virtualenv tool. Note that the version of pip installed in the test environment was up to date, and so it did not need upgraded.


## Python Scripts

The Python scripts have been radically rationalized and there are now just a few key scripts, with the remainder moved to the [```archived```](archived) directory. The main scripts are:

* [```ncc.py```](ncc.py) -- A kind of Swiss Army Knife script with many options to get-config, get, edit-config, pass in parameters for substitution, etc. Can be easily extended by users to have more edit-config templates or more named filter templates. Available content can be seen using the ```--list-templates``` and ```--list-filters``` parameters.

* [```ncc-filtered-get.py```](ncc-filtered-get.py) -- Very simple script that takes a subtree filter and does a get.

* [```ncc-get-all-schema.py```](ncc-get-all-schema.py) -- Script that attempts to download all the supported schema that the box has and tries to compile them, determine missing includes or imports, etc.

* [```ncc-get-schema.py```](ncc-get-schema.py) -- Script to get a single names schema and dup it to ```STDOUT```.

* [```ncc-simple-poller.py```](ncc-simple-poller.py) -- Script that polls a device on a specified cadence for a specified subtree or XPath filter.

* [```rc-xr.py```](rc-xr.py) -- Embryonic RESTCONF sample script using the Python ```requests``` library.


A couple of the scripts used to have other names, so, for backwards compatibilPythonity, the following symlinks currently exist:


* ```ncc-oc-bgp.py``` --> ```ncc.py```

* ```ncc-get-all-schema-new.py``` --> ```ncc-get-all-schema.py```


### Running The Scripts

The scripts mostly have a fairly common set of options for help, hostname, port, username and password. Just try running with the `--help` option.

### ncc.py

The script `ncc.py` presents a number of useful operations. If we look at its help:

> Note that the help text displayed here may be out of step with the actual code. Please run latest version of the script to ensure satisfaction!


```
$ python ncc.py --help
usage: ncc.py [-h] [--host HOST] [-u USERNAME] [-p PASSWORD] [--port PORT]
              [--mcuproxy MCUPROXY] [--mcuproxyport MCUPROXYPORT]
              [--timeout TIMEOUT] [-v] [--default-op DEFAULT_OP] [-w]
              [--snippets SNIPPETS] [--params PARAMS]
              [--params-file PARAMS_FILE]
              [-f FILTER | --named-filter NAMED_FILTER | -x XPATH]
              (-c | --is-supported IS_SUPPORTED | --list-templates | --list-filters | -g | --get-oper | --do-edits DO_EDITS [DO_EDITS ...])

Select your NETCONF operation and parameters:

optional arguments:
  -h, --help            show this help message and exit
  --host HOST           The IP address for the device to connect to (default
                        localhost)
  -u USERNAME, --username USERNAME
                        Username to use for SSH authentication (default
                        'Administrator')
  -p PASSWORD, --password PASSWORD
                        Password to use for SSH authentication (default
                        'UlafPAdm')
  --port PORT           Specify this if you want a non-default port (default
                        830)
  --mcuproxy MCUPROXY   The IP address for the MCU with the socks proxy
                        running
  --mcuproxyport MCUPROXYPORT
                        Specify this if you want to use a non-default SOCKS5
                        port (default 1080)
  --timeout TIMEOUT     NETCONF operation timeout in seconds (default 60)
  -v, --verbose         Exceedingly verbose logging to the console
  --default-op DEFAULT_OP
                        The NETCONF default operation to use (default 'merge')
  -w, --where           Print where script is and exit
  --snippets SNIPPETS   Directory where 'snippets' can be found; default is
                        location of script
  --params PARAMS       JSON-encoded string of parameters dictionaryfor
                        templates
  --params-file PARAMS_FILE
                        JSON-encoded file of parameters dictionary for
                        templates
  -f FILTER, --filter FILTER
                        NETCONF subtree filter
  --named-filter NAMED_FILTER
                        Named NETCONF subtree filter
  -x XPATH, --xpath XPATH
                        NETCONF XPath filter
  -c, --capabilities    Display capabilities of the device.
  --is-supported IS_SUPPORTED
                        Query the server capabilities to determine whether the
                        device claims to support YANG modules matching the
                        provided regular expression. The regex provided is not
                        automatically anchored to start or end. Note that the
                        regex supplied must be in a format valid for Python
                        and that it may be necessary to quote the argument.
  --list-templates      List out named edit-config templates
  --list-filters        List out named filters
  -g, --get-running     Get the running config
  --get-oper            Get oper data
  --do-edits DO_EDITS [DO_EDITS ...]
                        Execute a sequence of named templates with an optional
                        default operation and a single commit when candidate
                        config supported. If only writable-running support,
                        ALL operations will be attempted.
```

In subsequent sections some of its capabilities will be expanded on.

#### Device Capabilities

It is now possible to query the device either to return a categorized list of capabilities and models or to return the models matching a provided Python regular expression.

To get device capabilities:

```
$ ./ncc.py --host=192.168.1.2 --username=Administrator --password=mypwd --capabilities
IETF NETCONF Capabilities:
        urn:ietf:params:netconf:capability:writable-running:1.0
        urn:ietf:params:netconf:base:1.0
        urn:ietf:params:netconf:base:1.1
        urn:ietf:params:netconf:capability:with-defaults:1.0?basic-mode=report-all&also-supported=trim
IETF Models:
        ietf-netconf-with-defaults (urn:ietf:params:xml:ns:yang:ietf-netconf-with-defaults)
        ietf-inet-types (urn:ietf:params:xml:ns:yang:ietf-inet-types)
        ietf-yang-types (urn:ietf:params:xml:ns:yang:ietf-yang-types)
Albis-Elcon Models:
        acceed_644_1_33_0_LT (http://www.albis-elcon.com/ulaf/acceed_644_LT)
```

#### Snippets

Snippets are a way to pre-define edit-config messages or complex filters that you want to use from the command line. Snippets are simple Jinja2 templates, with parameters provided either from the command line or via a file.

Snippets are by default found in a directory  ```snippets``` colocated with the ```ncc.py``` script.
Named subtree filters are stored in [snippets/filters](snippets/filters) and named edit-config templates are stored in [snippets/editconfigs](snippets/editconfigs). The naming convention is fairly obvious; templates files end in ```.tmpl```, but when referred to via CLI arguments the extension is ommitted.

The snippets folder contains templates that can be used together with the albis-elcon ACCEED devices (https://albis-elcon.com)

```
snippets
├── editconfigs
└── filters
```


The original snippets have been moved into the folder snippets-orig and snippets-xe-orig. The command line option ```--snippets``` may be used to define an alternate location for the ```snippets``` directory.

The snippets for both edit config messages and named filters now support a JSON format for specifying parameters either on the command line of in a provided file. For example, we may have the filter snippet in file ```get_device_description.tmpl```:

```
<filter>
   <ulafne:Board xmlns:ulafne="http://www.albis-elcon.com/ulaf/acceed_{{FW_ID}}_{{BG_MODE}}">
     <ulafne:Local>
        <ulafne:Information>
        <ulafne:Description/>
        </ulafne:Information>
     </ulafne:Local>
   </ulafne:Board>
</filter>

```

Then, run against the ACCEED device:

```
$ ./ncc.py --host=192.168.1.2 --username=Administrator --password=mypwd --get-oper --named-filter get_device_description --params '{"FW_ID":"644","BG_MODE":"LT"}'
<data xmlns="urn:ietf:params:xml:ns:netconf:base:1.0" xmlns:nc="urn:ietf:params:xml:ns:netconf:base:1.0">
  <Board xmlns="http://www.albis-elcon.com/ulaf/acceed_644_LT">
    <Local>
      <Information>
        <Description>my device</Description>
      </Information>
    </Local>
  </Board>
</data>
```

Or if you want to have the template parameters in a separate file:

```
$ echo '{"FW_ID":"644","BG_MODE":"LT"}' > test_params.json
$ ./ncc.py --host=192.168.1.2 --username=Administrator --password=mypwd --get-oper --named-filter get_device_description --params-file test_params.json'
<data xmlns="urn:ietf:params:xml:ns:netconf:base:1.0" xmlns:nc="urn:ietf:params:xml:ns:netconf:base:1.0">
  <Board xmlns="http://www.albis-elcon.com/ulaf/acceed_644_LT">
    <Local>
      <Information>
        <Description>my device</Description>
      </Information>
    </Local>
  </Board>
</data>
```

If you do not supply all of the required vars, you will get an error when using the template.  For example in the above

```
$ ./ncc.py --host=192.168.1.2 --username=Administrator --password=mypwd --get-oper --named-filter get_device_description'
Undefined variable 'BG_MODE' is undefined.  Use --params to specify json dict
```

When edit-config templates or filters are listed (```--list-templates``` or ```--list-filters```), the variables that need to be substituted are also listed. For example:

```
$ ./ncc.py --list-filters
Named filters:
  get_device_description :{ "BG_MODE" : "","FW_ID" : "" }

$ ./ncc.py --list-templates
Edit-config templates:
  set_device_description :{ "BG_MODE" : "","Device_Descr" : "","FW_ID" : "" }

```

## Usage of the examples

Write device description to a ACCEED device:
```
./ncc.py --host=192.168.1.2 --username=Administrator --password=mypwd --do-edits set_device_description --params '{"Device_Descr":"my device", "FW_ID":"644", "BG_MODE":"LT"}'
```

Read device description from a ACCEED device:

```
$ ./ncc.py --host=192.168.1.2 --username=Administrator --password=mypwd --get-oper --named-filter get_device_description --params '{"FW_ID":"644","BG_MODE":"LT"}'
<data xmlns="urn:ietf:params:xml:ns:netconf:base:1.0" xmlns:nc="urn:ietf:params:xml:ns:netconf:base:1.0">
  <Board xmlns="http://www.albis-elcon.com/ulaf/acceed_644_LT">
    <Local>
      <Information>
        <Description>my device</Description>
      </Information>
    </Local>
  </Board>
</data>

```

## NETCONF connections over SOCKS5 proxy

The Ulaf+ V3 rack offers the possibility to connect via a SOCKS5 proxy to the line cards (and associated EFM-NT) in the slots 1..16. With the argument '''--mcuproxy''' ncc can used to perform NETCONF commands on the line cards.
The SOCKS5 proxy on the MCU needs not authentification. 

In the Ulaf + system the NETCONF connection to the line cards use the MCU credentials for authentication.

The following example shows the use of the --mcuproxy arguments:

```
./ncc.py --host=slot001  --username=Administrator --password=mypwd --mcuproxy=10.4.132.52 --capabilities
```

slot001 is the internal host of the line card in slot 1
10.4.132.52 is the IP address of the MCU
Administrator and mypwd are the credentials on the MCU

## host names of the line cards and EFM-NT

slot001 to slot016: host names of the line cards
slot00x_rem00y: host name of the remote devices connected to the line cards (if any)

