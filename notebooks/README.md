# Jupyter Notebooks Scripts

## Introduction

This folder contains a collection of Jupyter (IPython) Notebooks scripts that can be used together with the ACCEED devices from albis-elcon.

## Running The Jupyter Notebooks

The jupyter notebook server should be run inside the same Python virtualenv as you created above for running the Python scripts, with one addition, which is to run ```pip install jupyter``` in the virtual environment, as it is not currently listed in the [```requirements.txt```](requirements.txt) file.

Once Jupyter installed, the notebook server is run up thus:

```
$ . v/bin/activate
$ pip install jupyter
$ jupyter notebook
[I 16:39:38.230 NotebookApp] The port 8888 is already in use, trying another port.
[I 16:39:38.240 NotebookApp] Serving notebooks from local directory: /opt/git-repos/ncc
[I 16:39:38.240 NotebookApp] 0 active kernels
[I 16:39:38.240 NotebookApp] The Jupyter Notebook is running at: http://localhost:8889/
[I 16:39:38.240 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
```

When the notebook server is running, it will also open up a web page with your default web browser, pointing to the jupyter notebook server. Just pick one of the notebooks in the [notebooks](notebooks) directory (```*.ipynb```) and away you go!!

