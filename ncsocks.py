#!/usr/bin/env python

import errno
import urllib2
import socks
import socket
import os
from argparse import ArgumentParser
import sys
import time
from threading import Thread
import msvcrt

try:
    from Queue import Queue, Empty  # python 2.x
except ImportError:
    from queue import Queue, Empty  # python 3.x

msvcrt.setmode(sys.stdin.fileno(), os.O_BINARY)

class ReadAsync(object):
    def __init__(self, blocking_function, *args):
        self.args = args
        self.read = blocking_function

        self.thread = Thread(target=self.enqueue)
        self.queue = Queue()
        self.thread.daemon = True

        self.thread.start()

    def enqueue(self):
        while True:
            buf = self.read(*self.args)
            self.queue.put(buf)

    def dequeue(self):
        # Throws an exeption called Empty if there's no data to be read
        return self.queue.get_nowait()




parser = ArgumentParser(description='ncsocks establishes TCP connections over SOCKS5 proxy')

parser.add_argument('--host', type=str, default='127.0.0.1', required=True,
                        help="The IP address for the device to connect to (default localhost)")
parser.add_argument('--port', type=int, default=830,
                        help="Specify this if you want a non-default port (default 830)")
parser.add_argument('--mcuproxy', type=str, required=True,
                        help="The IP address for the MCU with the socks proxy running ")
parser.add_argument('--mcuproxyport', type=int, default=1080,
                        help="Specify this if you want to use a non-default SOCKS5 port (default 1080)")
    
args = parser.parse_args()
conn = socks.socksocket()
conn.set_proxy(socks.SOCKS5, args.mcuproxy, args.mcuproxyport)
conn.connect((args.host, args.port))
conn.setblocking(0)

stdin = ReadAsync(sys.stdin.buffer.read)

while True:
    try:
        os.write(sys.stdout.fileno(), conn.recv(4096))
    except socket.error,e:
        # POSIX: this error is raised to indicate no data available
        if e.errno != errno.EWOULDBLOCK:
            raise
    try:
        conn.send(stdin.dequeue())
    except Empty:
        time.sleep(0.1)

conn.close()
