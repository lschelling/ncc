﻿
Trap destination commands:
--------------------------


-----
-SET-
-----
--------------------------------------
-Set Trap_Destination über Netconfigc-
--------------------------------------

<?xml version='1.0' encoding='UTF-8'?>
<rpc xmlns="urn:ietf:params:xml:ns:netconf:base:1.0" message-id="10">
  <edit-config>
    <target>
      <running/>
    </target>
    <config>
      <ulafne:Board xmlns:ulafne="http://www.albis-elcon.com/ulaf/acceed_555_SA">
        <ulafne:Local>
          <ulafne:Management_Access>
            <ulafne:SNMP>
              <ulafne:Trap_Destination>
                <ulafne:Trap_Destination_ID>1</ulafne:Trap_Destination_ID>
                <ulafne:IP_Address>10.4.8.99</ulafne:IP_Address>
              </ulafne:Trap_Destination>
            </ulafne:SNMP>
          </ulafne:Management_Access>
        </ulafne:Local>
      </ulafne:Board>
    </config>
  </edit-config>
</rpc>

----------------------------------------
-ncc template: set_fix_trap_destination-
----------------------------------------
File<snippets/editconfigs/set_fix_trap_destination.tmpl
<config>
   <ulafne:Board xmlns:ulafne="http://www.albis-elcon.com/ulaf/acceed_555_SA">
     <ulafne:Local>
       <ulafne:Management_Access>
          <ulafne:SNMP>
            <ulafne:Trap_Destination>
              <ulafne:Trap_Destination_ID>1</ulafne:Trap_Destination_ID>
              <ulafne:IP_Address>10.4.8.99</ulafne:IP_Address>
            </ulafne:Trap_Destination>
          </ulafne:SNMP>
       </ulafne:Management_Access>
     </ulafne:Local>
   </ulafne:Board>
</config>

ncc call:
python ncc.py --host=10.4.177.10 --username=Administrator --password= --do-edits set_trap_destination

-----------------------------------
-ncc template: set_trap_destination-
-----------------------------------
File<snippets/editconfigs/set_trap_destination.tmpl
<config>
   <ulafne:Board xmlns:ulafne="http://www.albis-elcon.com/ulaf/acceed_{{FW_ID}}_{{BG_MODE}}">
     <ulafne:Local>
       <ulafne:Management_Access>
          <ulafne:SNMP>
            <ulafne:Trap_Destination>
              <ulafne:Trap_Destination_ID>{{TRAP_DESTINATION_ID}}</ulafne:Trap_Destination_ID>
              <ulafne:IP_Address>{{IP_ADDRESS}}</ulafne:IP_Address>
            </ulafne:Trap_Destination>
          </ulafne:SNMP>
       </ulafne:Management_Access>
     </ulafne:Local>
   </ulafne:Board>
</config>

ncc call:
python ncc.py --host=10.4.177.10 --username=Administrator --password= --do-edits set_trap_destination --params '{"FW_ID":"555", "BG_MODE":"SA", "TRAP_DESTINATION_ID":"1", "IP_ADDRESS":"10.4.8.8"}'

---------------------------
-Answer from ACCEED device-
---------------------------
<?xml version='1.0' encoding='UTF-8'?>
<rpc-reply xmlns="urn:ietf:params:xml:ns:netconf:base:1.0" message-id="6">
  <ok/>
</rpc-reply>


-----
-GET-
-----
--------------------------------------
-Get Trap_Destination über Netconfigc-
--------------------------------------
<?xml version='1.0' encoding='UTF-8'?>
<rpc xmlns="urn:ietf:params:xml:ns:netconf:base:1.0" message-id="4">
  <get-config>
    <source>
      <running/>
    </source>
    <filter type="subtree">
      <ulafne:Board xmlns:ulafne="http://www.albis-elcon.com/ulaf/acceed_555_SA">
        <ulafne:Local>
          <ulafne:Management_Access>
            <ulafne:SNMP>
              <ulafne:Trap_Destination/>
            </ulafne:SNMP>
          </ulafne:Management_Access>
        </ulafne:Local>
      </ulafne:Board>
    </filter>
  </get-config>
</rpc>

-----------------------------------
-ncc template get_trap_destination-
-----------------------------------
File<snippets/filters/get_trap_destination.tmpl
<filter>
   <ulafne:Board xmlns:ulafne="http://www.albis-elcon.com/ulaf/acceed_555_SA">
     <ulafne:Local>
       <ulafne:Management_Access>
          <ulafne:SNMP>
            <ulafne:Trap_Destination/>
          </ulafne:SNMP>
       </ulafne:Management_Access>
     </ulafne:Local>
   </ulafne:Board>
</filter>

ncc get call (all parameter):
python ncc.py --host=10.4.177.10 --username=Administrator --password= --get-oper --named-filter get_device_description --params '{"FW_ID":"555","BG_MODE":"SA"}'

ncc get-config call (exclude read only parameter):
python ncc.py --host=10.4.177.10 --username=Administrator --password= --get-running --named-filter get_device_description --params '{"FW_ID":"555","BG_MODE":"SA"}'

--------------------------------------------------
-Answer from ACCEED device for get-running config-
--------------------------------------------------
<?xml version='1.0' encoding='UTF-8'?>
<rpc-reply xmlns="urn:ietf:params:xml:ns:netconf:base:1.0" message-id="4">
  <data>
    <Board xmlns="http://www.albis-elcon.com/ulaf/acceed_555_SA">
      <Local>
        <Management_Access>
          <SNMP>
            <Trap_Destination>
              <Trap_Destination_ID>1</Trap_Destination_ID>
              <Description/>
              <IP_Address>10.4.8.8</IP_Address>
              <SNMP_Version>V1</SNMP_Version>
              <SNMP_Community>public</SNMP_Community>
              <Active_MIBs>Standard-MIBs ACCEED-MIB</Active_MIBs>
            </Trap_Destination>
            <Trap_Destination>
              <Trap_Destination_ID>2</Trap_Destination_ID>
              <Description/>
              <IP_Address>10.4.8.9</IP_Address>
              <SNMP_Version>V1</SNMP_Version>
              <SNMP_Community>public</SNMP_Community>
              <Active_MIBs>Standard-MIBs ACCEED-MIB</Active_MIBs>
            </Trap_Destination>
          </SNMP>
        </Management_Access>
      </Local>
    </Board>
  </data>
</rpc-reply>


--------
-DELETE-
--------
-----------------------------------------------
-Delete (set) Trap_Destination über Netconfigc-
-----------------------------------------------
<?xml version='1.0' encoding='UTF-8'?>
<rpc xmlns="urn:ietf:params:xml:ns:netconf:base:1.0" message-id="1">
  <edit-config>
    <target>
      <running/>
    </target>
    <config>
      <ulafne:Board xmlns:ulafne="http://www.albis-elcon.com/ulaf/acceed_555_SA">
        <ulafne:Local>
          <ulafne:Management_Access>
            <ulafne:SNMP>
              <ulafne:Trap_Destination>
                <ulafne:Trap_Destination_ID xmlns:xc="urn:ietf:params:xml:ns:netconf:base:1.0" xc:operation="delete">1</ulafne:Trap_Destination_ID>
                <ulafne:IP_Address>10.4.8.9</ulafne:IP_Address>
              </ulafne:Trap_Destination>
            </ulafne:SNMP>
          </ulafne:Management_Access>
        </ulafne:Local>
      </ulafne:Board>
    </config>
  </edit-config>
</rpc>


------------------------------------------
-ncc template delete_fix_trap_destination-
------------------------------------------
File<snippets/editconfigs/delete_fix_trap_destination.tmpl
<config>
  <ulafne:Board xmlns:ulafne="http://www.albis-elcon.com/ulaf/acceed_555_SA">
    <ulafne:Local>
      <ulafne:Management_Access>
        <ulafne:SNMP>
          <ulafne:Trap_Destination>
            <ulafne:Trap_Destination_ID operation="delete">1</ulafne:Trap_Destination_ID>
          </ulafne:Trap_Destination>
        </ulafne:SNMP>
      </ulafne:Management_Access>
    </ulafne:Local>
  </ulafne:Board>
</config>

ncc call:
python ncc.py --host=10.4.177.10 --username=Administrator --password= --do-edits delete_trap_destination

--------------------------------------
-ncc template delete_trap_destination-
--------------------------------------
File<snippets/editconfigs/delete_trap_destination.tmpl
<config>
  <ulafne:Board xmlns:ulafne="http://www.albis-elcon.com/ulaf/acceed_{{FW_ID}}_{{BG_MODE}}">
    <ulafne:Local>
      <ulafne:Management_Access>
        <ulafne:SNMP>
          <ulafne:Trap_Destination>
            <ulafne:Trap_Destination_ID operation="delete">{{TRAP_DESTINATION_ID}}</ulafne:Trap_Destination_ID>
          </ulafne:Trap_Destination>
        </ulafne:SNMP>
      </ulafne:Management_Access>
    </ulafne:Local>
  </ulafne:Board>
</config>

ncc call:
python ncc.py --host=10.4.177.10 --username=Administrator --password= --do-edits delete_trap_destination --params '{"FW_ID":"555", "BG_MODE":"SA", "TRAP_DESTINATION_ID":"1"}'

---------------------------
-Answer from ACCEED device-
---------------------------
<?xml version='1.0' encoding='UTF-8'?>
<rpc-reply xmlns="urn:ietf:params:xml:ns:netconf:base:1.0" message-id="8">
  <ok/>
</rpc-reply>


-----------------------------
-Prepare a new template file-
-----------------------------
python ncc.py --host=10.4.177.10 --username=Administrator --password= --get-running > acceed_all_config_template.tmpl
Replace the first line by <config> and the last line by </config>
python ncc.py --host=10.4.177.10 --username=Administrator --password= --do-edits acceed_all_config_template

Funktioniert nur wenn die Dateigrösse kleiner als ca. 4500Bytes ist.

----------------
-YANG DATAMODEL-
----------------
container Management_Access {
	container SNMP {
	  list Trap_Destination {
		key "Trap_Destination_ID";
		min-elements "0";
		max-elements "8";
		leaf Trap_Destination_ID {
		  type uint32 {
			range "1..8";
		  }
		  description
			"Trap destination ID";
		}
		leaf Description {
		  default "";
		  type string {
			length "0..32";
		  }
		  description
			"Description of this trap destination";
		}
		leaf IP_Address {
		  type string {
			length "1..128";
		  }
		  description
			"IP address of this trap destination";
		}
		leaf Port {
		  default "162";
		  type int32 {
			range "0..65535";
		  }
		  config "false";
		  description
			"UDP port of the trap receiver";
		}
		leaf SNMP_Version {
		  type EnumSnmpVersion;
		  description
			"Determines the trap format";
		}
		leaf SNMP_Community {
		  default "public";
		  type string {
			length "0..32";
		  }
		  description
			"SNMP community name";
		}
		leaf Active_MIBs {
		  type BitArrayActiveMIBs;
		  description
			"Only traps of active MIBs will be sent";
		}
	  }
...

